package za.ac.sun.cs.tcptester;

import java.lang.management.ManagementFactory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TcpTester {

	public static void main(String[] args) throws InterruptedException {
		String jvmName = ManagementFactory.getRuntimeMXBean().getName();
		Logger LOGGER = LogManager.getLogger(jvmName);
		LOGGER.info("I am the master");
		for (int i = 0; i < 20; i++) {
			System.out.println("Iteration: " + i);
			LOGGER.info("Iteration {}", i);
			Thread.sleep(1000);
		}
		LOGGER.info("Done");
	}

}
