package za.ac.sun.cs.tcplogger;

import java.nio.charset.Charset;

import org.apache.logging.log4j.core.parser.XmlLogEventParser;

public class XmlInputStreamLogEventBridge extends InputStreamLogEventBridge {
	
	private static final String EVENT_END = "</Event>";
	private static final String EVENT_START_NS_N = "<Event>";
	private static final String EVENT_START_NS_Y = "<Event ";
	
	public XmlInputStreamLogEventBridge() {
		this(1024, Charset.defaultCharset());
	}

	public XmlInputStreamLogEventBridge(final int bufferSize, final Charset charset) {
		super(new XmlLogEventParser(), bufferSize, charset, EVENT_END);
	}

	@Override
	protected int[] getEventIndices(final String text, final int beginIndex) {
		int start = text.indexOf(EVENT_START_NS_Y, beginIndex);
		int startLen = EVENT_START_NS_Y.length();
		if (start < 0) {
			start = text.indexOf(EVENT_START_NS_N, beginIndex);
			startLen = EVENT_START_NS_N.length();
		}
		final int end = start < 0 ? -1 : text.indexOf(EVENT_END, start + startLen);
		return new int[] { start, end };
	}

}
