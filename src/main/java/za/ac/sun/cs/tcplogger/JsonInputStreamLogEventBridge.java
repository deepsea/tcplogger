package za.ac.sun.cs.tcplogger;

import java.nio.charset.Charset;

import org.apache.logging.log4j.core.parser.JsonLogEventParser;
import org.apache.logging.log4j.util.Chars;

public class JsonInputStreamLogEventBridge extends InputStreamLogEventBridge {
	
	private static final int[] END_PAIR = new int[] { END, END };
	private static final char EVENT_END_MARKER = '}';
	private static final char EVENT_START_MARKER = '{';
	private static final char JSON_ESC = '\\';
	private static final char JSON_STR_DELIM = Chars.DQUOTE;
	
	public JsonInputStreamLogEventBridge() {
		this(1024, Charset.defaultCharset());
	}

	public JsonInputStreamLogEventBridge(final int bufferSize, final Charset charset) {
		super(new JsonLogEventParser(), bufferSize, charset, String.valueOf(EVENT_END_MARKER));
	}

	@Override
	protected int[] getEventIndices(final String text, final int beginIndex) {
		final int start = text.indexOf(EVENT_START_MARKER, beginIndex);
		if (start == END) {
			return END_PAIR;
		}
		final char[] charArray = text.toCharArray();
		int stack = 0;
		boolean inStr = false;
		boolean inEsc = false;
		for (int i = start; i < charArray.length; i++) {
			final char c = charArray[i];
			if (inEsc) {
				inEsc = false;
			} else {
				switch (c) {
				case EVENT_START_MARKER:
					if (!inStr) {
						stack++;
					}
					break;
				case EVENT_END_MARKER:
					if (!inStr) {
						stack--;
					}
					break;
				case JSON_STR_DELIM:
					inStr = !inStr;
					break;
				case JSON_ESC:
					inEsc = true;
					break;
				}
				if (stack == 0) {
					return new int[] { start, i };
				}
			}
		}
		return END_PAIR;
	}

}
