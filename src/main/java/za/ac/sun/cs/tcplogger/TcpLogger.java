package za.ac.sun.cs.tcplogger;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OptionalDataException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.apache.logging.log4j.core.parser.ParseException;
import org.apache.logging.log4j.core.util.Closer;
import org.apache.logging.log4j.core.util.Log4jThread;
import org.apache.logging.log4j.message.EntryMessage;

public class TcpLogger<T extends InputStream> extends AbstractSocketServer<T> {
	
	private class SocketHandler extends Log4jThread {
		
		private final T inputStream;
		
		private final Socket socket;
		
		private volatile boolean shutdown = false;
		
		public SocketHandler(final Socket socket) throws IOException {
			this.socket = socket;
			this.inputStream = logEventInput.wrapStream(socket.getInputStream());
		}
		
		@Override
		public void run() {
			final EntryMessage entry = logger.traceEntry();
			boolean closed = false;
			try {
				try {
					while (!shutdown) {
						logEventInput.logEvents(inputStream, TcpLogger.this);
					}
				} catch (final EOFException e) {
					closed = true;
				} catch (final OptionalDataException e) {
					logger.error("OptionDataException eof=" + e.eof + " length=" + e.length, e);
				} catch (final IOException e) {
					logger.error("IOException encountered while reading from socket", e);
				} catch (ParseException e) {
					logger.error("ParseException encountered while reading from socket", e);
				}
				if (!closed) {
					Closer.closeSilently(inputStream);
				}
			} finally {
				handlers.remove(Long.valueOf(getId()));
			}
			logger.traceExit(entry);
		}
		
		public void shutdown() {
			this.shutdown = true;
			if (socket != null) {
				Closer.closeSilently(socket);
			}
			interrupt();
		}
	}

	public static TcpLogger<InputStream> createJSonSocketServer(final int port) throws IOException {
		LOGGER.entry("createJSonSocketServer", port);
		final TcpLogger<InputStream> socketServer = new TcpLogger<>(port, new JsonInputStreamLogEventBridge());
		return LOGGER.exit(socketServer);
	}

	public static TcpLogger<InputStream> createJsonSocketServer(final int port, final int backlog, final InetAddress localBindAddress) throws IOException {
		LOGGER.entry("createJsonSocketServer", port, backlog, localBindAddress);
		final TcpLogger<InputStream> socketServer = new TcpLogger<>(port, backlog, localBindAddress, new JsonInputStreamLogEventBridge());
		return LOGGER.exit(socketServer);
	}
	
	public static TcpLogger<InputStream> createXmlSocketSever(final int port) throws IOException {
		LOGGER.entry(port);
		final TcpLogger<InputStream> socketServer = new TcpLogger<>(port, new XmlInputStreamLogEventBridge());
		return LOGGER.exit(socketServer);
	}
	
	public static TcpLogger<InputStream> createXmlSocketSever(final int port, final int backlog, final InetAddress localBindAddress) throws IOException {
		LOGGER.entry(port);
		final TcpLogger<InputStream> socketServer = new TcpLogger<>(port, backlog, localBindAddress, new XmlInputStreamLogEventBridge());
		return LOGGER.exit(socketServer);
	}

	public static void main(final String[] args) throws Exception {
		final TcpLogger<InputStream> socketServer = TcpLogger.createJsonSocketServer(9987, 50, null);
		/*final Thread serverThread =*/ socketServer.startNewThread();
	}

	private final ConcurrentMap<Long, SocketHandler> handlers = new ConcurrentHashMap<>();
	
	private final ServerSocket serverSocket;
	
	@SuppressWarnings("resource")
	public TcpLogger(final int port, final int backlog, final InetAddress localBindAddress, final LogEventBridge<T> logEventInput) throws IOException {
		this(port, logEventInput, new ServerSocket(port, backlog, localBindAddress));
	}
	
	@SuppressWarnings("resource")
	public TcpLogger(final int port, final LogEventBridge<T> logEventInput) throws IOException {
		this(port, logEventInput, new ServerSocket(port));
	}

	@SuppressWarnings("resource")
	public TcpLogger(final int port, final LogEventBridge<T> logEventInput, final ServerSocket serverSocket) throws IOException {
		super(port, logEventInput);
		this.serverSocket = serverSocket;
	}

	@Override
	public void run() {
		final EntryMessage entry = logger.traceEntry();
		while (isActive()) {
			if (serverSocket.isClosed()) {
				return;
			}
			try {
				logger.debug("Listening for a connection {}...", serverSocket);
				@SuppressWarnings("resource")
				final Socket clientSocket = serverSocket.accept();
				logger.debug("Accepted connection on {}...", serverSocket);
				logger.debug("Socket accepted: {}", clientSocket);
				clientSocket.setSoLinger(true, 0);
				final SocketHandler handler = new SocketHandler(clientSocket);
				handlers.put(Long.valueOf(handler.getId()), handler);
				handler.start();
			} catch (final IOException e) {
				if (serverSocket.isClosed()) {
					logger.traceExit(entry);
					return;
				}
				logger.error("Exception encountered on accept. Ignoring. Stack trace:", e);
			}
		}
		for (final Map.Entry<Long, SocketHandler> handlerEntry : handlers.entrySet()) {
			final SocketHandler handler = handlerEntry.getValue();
			handler.shutdown();
			try {
				handler.join();
			} catch (final InterruptedException ignored) {
				// ignore the exception
			}
		}
		logger.traceExit(entry);
	}

	@Override
	public void shutdown() throws IOException {
		final EntryMessage entry = logger.traceEntry();
		setActive(false);
		serverSocket.close();
		logger.traceExit(entry);
	}

	@Override
	public String toString() {
		return "TcpLogger [serverSocket=" + serverSocket + ", handlers=" + handlers + ", logEventInput=" + logEventInput + "]";
	}

}
