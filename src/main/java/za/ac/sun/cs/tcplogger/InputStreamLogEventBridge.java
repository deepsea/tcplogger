package za.ac.sun.cs.tcplogger;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.LogEventListener;
import org.apache.logging.log4j.core.parser.ParseException;
import org.apache.logging.log4j.core.parser.TextLogEventParser;
import org.apache.logging.log4j.util.Strings;

public abstract class InputStreamLogEventBridge extends AbstractLogEventBridge<InputStream> {

	private final int bufferSize;
	
	private final Charset charset;
	
	private final String eventEndMarker;
	
	private final TextLogEventParser parser;
	
	public InputStreamLogEventBridge(final TextLogEventParser parser, final int bufferSize, final Charset charset, final String eventEndMarker) {
		this.bufferSize = bufferSize;
		this.charset = charset;
		this.eventEndMarker = eventEndMarker;
		this.parser = parser;
	}
	
	abstract protected int[] getEventIndices(final String text, int beginIndex);
	
	@Override
	public void logEvents(final InputStream inputStream, final LogEventListener logEventListener) throws IOException, ParseException {
		String workingText = Strings.EMPTY;
		try {
			final byte[] buffer = new byte[bufferSize];
			String textRemains = workingText = Strings.EMPTY;
			while (true) {
				final int streamReadLength = inputStream.read(buffer);
				if (streamReadLength == END) {
					break;
				}
				final String text = workingText = textRemains + new String(buffer, 0, streamReadLength, charset);
				int beginIndex = 0;
				while (true) {
					final int[] pair = getEventIndices(text, beginIndex);
					final int eventStartMarkerIndex = pair[0];
					if (eventStartMarkerIndex < 0) {
						textRemains = text.substring(beginIndex);
						break;
					}
					final int eventEndMarkerIndex = pair[1];
					if (eventEndMarkerIndex > 0) {
						final int eventEndXmlIndex = eventEndMarkerIndex + eventEndMarker.length();
						final String textEvent = workingText = text.substring(eventStartMarkerIndex, eventEndXmlIndex);
						final LogEvent logEvent = unmarshal(textEvent);
						logEventListener.log(logEvent);
						beginIndex = eventEndXmlIndex;
					} else {
						textRemains = text.substring(beginIndex);
						break;
					}
				}
			}
		} catch (final IOException e) {
			logger.error(workingText, e);
		}
	}
	
	protected LogEvent unmarshal(final String jsonEvent) throws ParseException {
		return this.parser.parseFrom(jsonEvent);
	}

	@Override
	public String toString() {
		return "InputStreamLogEventBridge [bufferSize=" + bufferSize + ", charset=" + charset + ", eventEndMarker=" + eventEndMarker + ", parser=" + parser + "]";
	}

}
