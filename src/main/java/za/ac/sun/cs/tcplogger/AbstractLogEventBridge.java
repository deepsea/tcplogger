package za.ac.sun.cs.tcplogger;

import java.io.IOException;
import java.io.InputStream;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.status.StatusLogger;

public abstract class AbstractLogEventBridge<T extends InputStream> implements LogEventBridge<T> {

	protected static final int END = -1;
	
	protected static final Logger logger = StatusLogger.getLogger();
	
	@SuppressWarnings("unchecked")
	@Override
	public T wrapStream(final InputStream inputStream) throws IOException {
		return (T) inputStream;
	}

}
