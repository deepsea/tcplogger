package za.ac.sun.cs.tcplogger;

import java.io.IOException;
import java.io.InputStream;

import org.apache.logging.log4j.core.LogEventListener;
import org.apache.logging.log4j.core.parser.ParseException;

public interface LogEventBridge<T extends InputStream> {

	void logEvents(T inputStream, LogEventListener logEventListener) throws IOException, ParseException;
	
	T wrapStream(InputStream inputStream) throws IOException;
	
}
