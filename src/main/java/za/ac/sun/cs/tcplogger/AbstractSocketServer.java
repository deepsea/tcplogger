package za.ac.sun.cs.tcplogger;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Objects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LogEventListener;
import org.apache.logging.log4j.core.util.Log4jThread;

public abstract class AbstractSocketServer<T extends InputStream> extends LogEventListener implements Runnable {

	protected static final int MAX_PORT = 65534;
	
	private volatile boolean active = true;
	
	protected final LogEventBridge<T> logEventInput;
	
	protected final Logger logger;
	
	public AbstractSocketServer(final int port, final LogEventBridge<T> logEventInput) {
		this.logger = LogManager.getLogger(this.getClass().getName() + '.' + port);
		this.logEventInput = Objects.requireNonNull(logEventInput, "LogEventInput");
	}

	protected boolean isActive() {
		return this.active;
	}
	
	protected void setActive(final boolean isActive) {
		this.active = isActive;
	}

	public Thread startNewThread() {
		final Thread thread = new Log4jThread(this);
		thread.start();
		return thread;
	}

	public abstract void shutdown() throws Exception;
	
	public void awaitTermination(final Thread serverThread) throws Exception {
		final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		while (true) {
			final String line = reader.readLine();
			if (line == null
					|| line.equalsIgnoreCase("quit")
					|| line.equalsIgnoreCase("stop")
					|| line.equalsIgnoreCase("exit")) {
				this.shutdown();
				serverThread.join();
				break;
			}
		}
	}
	
}
